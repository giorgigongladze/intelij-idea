import kotlin.math.sqrt
import kotlin.math.pow

class Point(var x: Int, var y: Int ) {

    override fun toString(): String {
        return("$x, $y")
    }

    override fun equals(other: Any?): Boolean {
        if(other is Point) {
            if(x == other.x && y == other.y) {
                return true
            }
        }
        return false
     }

}

fun main() {
    var obj = Point(1,2)
    var obj2 = Point(5,3)
    var f1 = Fraction(7.0,4.0)
    var f2 = Fraction(5.0,8.0)
}

class Fraction(var numerator:Double = 0.0, var denominator:Double = 0.0) {

    override fun equals(other: Any?): Boolean {
        if(other is Fraction) {
            if(numerator / denominator == other.numerator / other.denominator) {
                return true
            }
        }
        return false

    }

    fun subtractFraction(anotherFraction: Fraction ): Double {
        return (numerator / denominator) - (anotherFraction.numerator / anotherFraction.denominator)
    }

    fun addFraction(anotherFraction: Fraction ): Double {
        return (numerator / denominator) + (anotherFraction.numerator / anotherFraction.denominator)
    }

    fun divideFraction(anotherFraction: Fraction ): Double {
        return (numerator / denominator) / (anotherFraction.numerator / anotherFraction.denominator)
    }

    fun multiplyFraction(anotherFraction: Fraction ): Double {
        return (numerator / denominator) * (anotherFraction.numerator / anotherFraction.denominator)
    }

}